/*
    1. setTimeout() спрацьовує один раз через певний інтервал часу, setInterval() - спрацьовує періодично через зазначений інтервал часу.

    2. Це планує виклик func настільки швидко, наскільки це можливо. Але планувальник викликатиме функцію лише після завершення виконання поточного коду.
       Так виклик функції буде заплановано одразу після виконання поточного коду.

    3. Для setInterval функція залишається в пам'яті доти, доки не буде викликано clearInterval. Є й побічний ефект. 
       Функція посилається на зовнішнє лексичне оточення, тому доки вона існує, зовнішні змінні існують також. 
       Вони можуть займати більше пам'яті, ніж функція. Тому, якщо регулярний виклик функції більше не потрібен, 
       краще відмінити його, навіть якщо функція дуже маленька.
 */

window.addEventListener('DOMContentLoaded', () => {
    const images = document.querySelectorAll('.image-to-show'),
          btnStop = document.querySelector('.btn-stop'),
          btnPlay = document.querySelector('.btn-play');
    let currentIndex = 0,
        intervalId = null,
        isPlaying = true;
    
    function showImage(index) {
        images.forEach((image, i) => {
            if (i === index) {
                image.classList.add('image-visible');
            } else {
                image.classList.remove('image-visible');
            }
        });
    }

    function startSlidesShow() {
        intervalId = setInterval(() => {
            currentIndex = (currentIndex + 1) % images.length;
            showImage(currentIndex);
        }, 3000);
    }

    showImage(currentIndex);
    startSlidesShow();

    btnStop.addEventListener('click', () => {
        clearInterval(intervalId);
        isPlaying = false;
    });

    btnPlay.addEventListener('click', () => {

        if (!isPlaying) {
            isPlaying = true;
            startSlidesShow();
        }
    });

});